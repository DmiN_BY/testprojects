﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theme06homework
{
    static class Homework06methods
    {
        public static string Task01(string input, char symbol)
        {
            string result;
            int symbolCounter = 0;
            for (int i = 0; i < input.Length; ++i)
                if (input[i] == symbol)
                    ++symbolCounter;
            result = "Symbol " 
                + symbol 
                + " was count " 
                + symbolCounter.ToString() 
                + " times.";
            return result;
        }

        public static string Task02(string input)
        {
            string result;
            string[] words = input.Split(' ');
            int maxWordLength = 0;
            string longestWord = " ";
            int countOfLongestWords = 0;
            foreach (string s in words)
            {
                if (s.Length >= maxWordLength)
                {
                    maxWordLength = s.Length;
                    longestWord = s;
                }
            }
            foreach (string s in words)
                if (s == longestWord)
                    ++countOfLongestWords;
            result = "Longest word is " 
                + longestWord 
                + ", count " 
                + countOfLongestWords.ToString() 
                + " times.";
            return result;
        }

        public static string Task03(string input)
        {
            string result = "";
            int startQuotesPosition = 0;
            int endQuotesPosition = 0;
            for (int i = 0; i < input.Length; ++i)
                if (input[i] == '"')
                {
                    if (startQuotesPosition > endQuotesPosition)
                        endQuotesPosition = i;
                    else
                        startQuotesPosition = i;
                }
            if (endQuotesPosition == 0)
            {
                if (startQuotesPosition == 0)
                    result = "No any quotes in string.";
                else
                    result = "Only one quote in the string.";
            }
            else
            {
                for (int i = startQuotesPosition; i <= endQuotesPosition; ++i)
                    result +=input[i];
            }
            return result;
        }

        public static string Task04(string input)
        {
            string result;
            int elementAsInt;
            string[] numeric = new string[10] {
                "zero","one","two","three", "four", "five", "six", "seven", "eight", "nine" };
            StringBuilder sb = new StringBuilder(input);
            int i = 0;
            do
            {
                if ((sb[i] >= '0') && (sb[i] <= '9'))
                {
                    elementAsInt = Convert.ToInt32(sb[i].ToString());
                    sb.Remove(i, 1);
                    sb.Insert(i, numeric[elementAsInt]);
                }
                ++i;
            }
            while (i < sb.Length);
            result = sb.ToString();
            return result;
        }
    }
}
