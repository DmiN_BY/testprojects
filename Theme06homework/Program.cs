﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Theme06homework
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            char symbol = 's';

            Console.Write("Enter string: ");
            input = Console.ReadLine();

            Console.WriteLine(Homework06methods.Task01(input, symbol));
            Console.WriteLine(Homework06methods.Task02(input));
            Console.WriteLine(Homework06methods.Task03(input));
            Console.WriteLine(Homework06methods.Task04(input));
        }
    }
}
